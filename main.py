import  ctypes
from pynput.keyboard import Key, Listener
# https://docs.microsoft.com/ru-ru/windows/win32/api/winuser/nf-winuser-mouse_event?redirectedfrom=MSDN

MOUSEEVENTF_MOVE = 0x0001 # mouse move 
MOUSEEVENTF_LEFTDOWN = 0x0002 # left button down 
MOUSEEVENTF_LEFTUP = 0x0004 # left button up 
MOUSEEVENTF_RIGHTDOWN = 0x0008 # right button down 
MOUSEEVENTF_RIGHTUP = 0x0010 # right button up 
MOUSEEVENTF_MIDDLEDOWN = 0x0020 # middle button down 
MOUSEEVENTF_MIDDLEUP = 0x0040 # middle button up 
MOUSEEVENTF_WHEEL = 0x0800 # wheel button rolled 
MOUSEEVENTF_ABSOLUTE = 0x8000 # absolute move 

def on_press(key):
    # print('{0} pressed'.format(key))
    if str(key) == "'a'":
        pointerMoveTo(1000, 100)

def on_release(key):
    print('{0} release'.format(key))
    if key == Key.esc:
        return False # Stop listener

def pointerMoveBy():
    ctypes.windll.user32.mouse_event(MOUSEEVENTF_MOVE, 100, 100, 0, 0)

def pointerMoveTo(x, y):
    ctypes.windll.user32.SetCursorPos(x, y)

def leftClick():
    ctypes.windll.user32.mouse_event(0x2, 0,0,0,0)    # Mouse LClick Down, relative coords, dx=0, dy=0
    ctypes.windll.user32.mouse_event(0x4, 0,0,0,0)    # Mouse LClick Up, relative coords, dx=0, dy=0

if __name__ == "__main__":  
    # Collect events until released
    with Listener(on_press=on_press, on_release=on_release) as listener:
        listener.join()

